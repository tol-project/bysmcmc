//////////////////////////////////////////////////////////////////////////////
// FILE    : build.node.obs.tol
// PURPOSE : observational level building methods
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//Returns common information about a time serie that will be used as output
//or input of a dynamic observational node
NameBlock Obs.Serie.Info(
  //Node identifier                           
  Text id_node,                           
  //Time serie identifier must be unique along whole hierarchy
  Text name,
  //How a data owner acts in a model (See BysMcmc::Options::Data.Owner.Type)
  Real ownerType,
  //Time serie data  
  Serie serie, 
  //Number of needed initial values
  Real numIniVal,
  //The first date to be analyzed
  Date first, 
  //The last date to be analyzed
  Date last, 
  //Missing label is used as prefix of missing parameters
  Text missing.label,
  //If there are missing values it will be added prior information about it
  //multiplying the standard deviation of the serie by this factor. 
  //If you don't have prior information about this set it to +1/0
  Real missing.sigmaFactor, 
  //Minimum feasible value for missing values. 
  //If you don't have prior information about this set it to -1/0
  Real missing.lowerBound, 
  //Maximum feasible value for missing values. 
  //If you don't have prior information about this set it to +1/0
  Real missing.upperBound)
//////////////////////////////////////////////////////////////////////////////
{[[
  Text _.id_node = id_node;
  Text _.name = name;
  Real _.ownerType = ownerType;
  Real _.isTimeSerie = True;
  Serie _.serie = serie;
  Real _.avg = 
  {
    Real aux = AvrS(_.serie);
    If(IsUnknown(aux), WriteLn("Serie "+_.id_node+"::"+_.name+
      " is fully unknown","W"));
    aux
  };
  Real _.stds = StDsS(_.serie);
  Real _.missing.sigmaFactor = missing.sigmaFactor; 
  Real _.missing.lowerBound  = missing.lowerBound; 
  Real _.missing.upperBound  = missing.upperBound;
  Text _.missing.label       = missing.label;
  Text _.missing.name        = missing.label+"_"+_.name;
  Text _.missing.id          = missing.label+"::"+_.name;

  //Number of needed initial values
  Real _.numIniVal = numIniVal;
  //The first date of initial values when needed
  Date _.firstIniVal = Succ(first,Dating(_.serie),-numIniVal);
  //The last date of initial values when needed
  Date _.lastIniVal = Succ(first,Dating(_.serie),-1);
  //The first date to be analyzed
  Date _.firstDate = first;
  //The last date to be analyzed
  Date _.lastDate  = last;
  //Sorted list of analyzed dates
  Set _.dates = Dates(Dating(_.serie), _.firstIniVal, _.lastDate);
  Matrix Get.Matrix(Real backwardOrder)
  {
    Serie s = (B^backwardOrder):_.serie;
    Tra(SerSetMat([[s]], _.firstDate, _.lastDate))
  };
  Real Get.Equation.Size(Real unused) 
  { 
    Card(_.dates) 
  };
  Serie _.unk.left = SubSer(CalInd(Dating(_.serie),Dating(_.serie)), 
                       _.firstIniVal, Succ(First(_.serie),Dating(_.serie),-1));
  Serie _.unk.right = SubSer(CalInd(Dating(_.serie),Dating(_.serie)), 
                       Succ(Last(_.serie),Dating(_.serie),+1),_.lastDate);
  //TimeSet of missing values
  Serie _.missing.serie = _.unk.left<<IsUnknown(_.serie)>>_.unk.right;
  TimeSet _.missing.dating = SerTms(_.missing.serie);
  //Sorted list of dates of missing values
  Set _.missing.dates = Dates(_.missing.dating, _.firstIniVal, _.lastDate);
//Real _trace.1 = { WriteLn("TRACE [Obs.Serie.Info] 1 _.name = "<<_.name); 0};
//Real _trace.2 = { WriteLn("TRACE [Obs.Serie.Info] 2 _.avg = "<<_.avg); 0};
//Real _trace.3 = { WriteLn("TRACE [Obs.Serie.Info] 3 _.stds = "<<_.stds); 0};
//Real _trace.4 = { WriteLn("TRACE [Obs.Serie.Info] 4 _.missing.sigmaFactor = "<<_.missing.sigmaFactor); 0};
//Real _trace.5 = { WriteLn("TRACE [Obs.Serie.Info] 5 _.missing.lowerBound = "<<_.missing.lowerBound); 0};
//Real _trace.6 = { WriteLn("TRACE [Obs.Serie.Info] 6 _.missing.upperBound = "<<_.missing.upperBound); 0};
//Real _trace.7 = { WriteLn("TRACE [Obs.Serie.Info] 7 _.firstDate = "<<_.firstDate); 0};
//Real _trace.8 = { WriteLn("TRACE [Obs.Serie.Info] 8 _.lastDate = "<<_.lastDate); 0};
//Real _trace.9 = { WriteLn("TRACE [Obs.Serie.Info] 9 _.missing.dates = "<<_.missing.dates); 0};
  //Returns a unique identifier for a missing value
  Text missing.name(Date dte)
  {
    Text txt = FormatDate(dte,"%cy%Ym%md%dh%hi%is%s");
    "MissingBlk::"+_.missing.id+"::"+txt
  };
  //Returns the expression of the t-th regression coefficient that could be a
  //a known real datum or the identifier of a missing value
  Text get.coef(Real t)
  {
  //WriteLn("TRACE [Obs.Serie.Info get.coef] 1 t="<<t);
    Date dte = _.dates[t+_.numIniVal];
  //WriteLn("TRACE [Obs.Serie.Info get.coef] 2 dte="<<dte);
    Real x = SerDat(_.serie, dte);
  //WriteLn("TRACE [Obs.Serie.Info get.coef] 3 x="<<x);
    If(IsUnknown(x),
       "+"+missing.name(dte),
       BysMcmc::Bsr::Import::RealTextS(x))
  };
  Text get.knownCoefOrAvg(Real t)
  {
  //WriteLn("TRACE [Obs.Serie.Info get.knownCoefOrAvg] 1 t="<<t);
    Date dte = _.dates[t+_.numIniVal];
  //WriteLn("TRACE [Obs.Serie.Info get.knownCoefOrAvg] 2 dte="<<dte);
    Real x = SerDat(_.serie, dte);
  //WriteLn("TRACE [Obs.Serie.Info get.knownCoefOrAvg] 3 x="<<x);
    BysMcmc::Bsr::Import::RealTextS(If(IsUnknown(x),_.avg, x))
  };
  //Sorted list of variables related to missing values
  Set _.missing = 
  {
  //WriteLn("TRACE [Obs.Serie.Info get.missing] 1 dte="<<dte);
    Set aux = For(1,Card(_.missing.dates),Set get.missing(Real num)
    {
      Date dte = _.missing.dates[num];
      Real x = SerDat(_.serie,dte);
      If(!IsUnknown(x),Copy(Empty),
      {
        Set mis = BysMcmc::@Bsr.Missing.Info(
          Text Name = missing.name(dte),
          Real Owner.Type = _.ownerType, 
          Text Owner.Name = _.id_node+"::"+_.name, 
          Real Owner.Column = ?,
          Real Owner.Row = 1+DateDif(Dating(_.serie),_.firstDate,dte),
          Real Prior.Average = _.avg, 
          Real Prior.Sigma = _.stds*_.missing.sigmaFactor, 
          Real Prior.LowerBound = _.missing.lowerBound, 
          Real Prior.UpperBound = _.missing.upperBound
        );
        Eval(ToName(mis->Name)+"=mis")
      })
    });
    Select(aux,Card)
  }
]]};

//////////////////////////////////////////////////////////////////////////////
//Returns common information about a vector that will be used as output
//or input of a dynamic observational node
NameBlock Obs.Vector.Info(
  //Node identifier                           
  Text id_node,                           
  //Time serie identifier must be unique along whole hierarchy
  Text name,
  //How a data owner acts in a model (See BysMcmc::Options::Data.Owner.Type)
  Real ownerType,
  //Time serie data
  Matrix serie, 
  //Missing label is used as prefix of missing parameters
  Text missing.label,
  //If there are missing values it will be added prior information about it
  //multiplying the standard deviation of the serie by this factor. 
  //If you don't have prior information about this set it to +1/0
  Real missing.sigmaFactor, 
  //Minimum feasible value for missing values. 
  //If you don't have prior information about this set it to -1/0
  Real missing.lowerBound, 
  //Maximum feasible value for missing values. 
  //If you don't have prior information about this set it to +1/0
  Real missing.upperBound)
//////////////////////////////////////////////////////////////////////////////
{[[
  Text _.id_node = id_node;
  Text _.name = name;
  Real _.ownerType = ownerType;
  Real _.isTimeSerie = False;
  Matrix _.serie = serie;
  Real _.avg = 
  {
    Real aux = MatAvr(_.serie);
    If(IsUnknown(aux), WriteLn("Vector "+_.name+" is fully unknown","W"));
    aux
  };
  Real _.stds = MatStDs(_.serie);
  Real _.missing.sigmaFactor = missing.sigmaFactor; 
  Real _.missing.lowerBound  = missing.lowerBound; 
  Real _.missing.upperBound  = missing.upperBound;
  Text _.missing.label       = missing.label;
  Text _.missing.name        = missing.label+"_"+_.name;
  Text _.missing.id          = missing.label+"::"+_.name;

  //Number of needed initial values
  Real _.numIniVal = numIniVal;
  //Returns a unique identifier for a missing value
  Text missing.name(Real num)
  {
    Text txt = IntText(num);
    "MissingBlk::"+_.missing.id+"::N"+txt
  };
  Matrix Get.Matrix(Real backwardOrder)
  {
    Real If(backwardOrder>0,
    {
      WriteLn("[Obs.Vector.Info] Backward polynomials are not allowed for matrices","E");
      Real Stop
    });
    _.serie
  };
  Real Get.Equation.Size(Real unused) 
  { 
    Rows(_.serie) 
  };
  //Returns the expression of the t-th regression coefficient that could be a
  //a known real datum or the identifier of a missing value
  Text get.coef(Real t)
  {
  //WriteLn("TRACE get.coef t="<<t);
    Real x = MatDat(_.serie, t, 1);
    If(IsUnknown(x),
       "+"+missing.name(num),
       BysMcmc::Bsr::Import::RealTextS(x))
  };
  Text get.knownCoefOrAvg(Real t)
  {
  //WriteLn("TRACE get.knownCoefOrAvg t="<<t);
    Real x = MatDat(_.serie, t,1);
    BysMcmc::Bsr::Import::RealTextS(If(IsUnknown(x),_.avg, x))
  };
  //Sorted list of variables related to missing values
  Set _.missing = 
  {
    Set aux = EvalSet(Range(1,Rows(_.serie),1),Set(Real num)
    {
      Real x = MatDat(_.serie,num,1);
      If(!IsUnknown(x),Copy(Empty),
      {
        BysMcmc::@Bsr.Missing.Info(
          Text Name = missing.name(num), 

          Real Owner.Type = _.ownerType, 
          Text Owner.Name = _.id_node+"::"+_.name, 
          Real Owner.Column = ?,
          Real Owner.Row = num,
          Real Prior.Average = _.avg, 
          Real Prior.Sigma = _.stds*_.missing.sigmaFactor, 
          Real Prior.LowerBound = _.missing.lowerBound, 
          Real Prior.UpperBound = _.missing.upperBound),
        
        Eval(ToName(mis->Name)+"=mis")
      })
    });
    Select(aux,Card)
  }
]]};

//////////////////////////////////////////////////////////////////////////////
//Returns common information about a time serie that will be used as output
//or input of a dynamic observational node
NameBlock Obs.Data.Info(
  //Node identifier                           
  Text id_node,                           
  //Time serie identifier must be unique along whole hierarchy
  Text name,
  //How a data owner acts in a model (See BysMcmc::Options::Data.Owner.Type)
  Real ownerType,
  //Time serie or vector data
  Anything serie, 
  //Number of needed initial values
  Real numIniVal,
  //The first date to be analyzed or UnknownDate if it's a vector
  Date first, 
  //The last date to be analyzed or UnknownDate if it's a vector
  Date last, 
  //Missing label is used as prefix of missing parameters
  Text missing.label,
  //If there are missing values it will be added prior information about it
  //multiplying the standard deviation of the serie by this factor. 
  //If you don't have prior information about this set it to +1/0
  Real missing.sigmaFactor, 
  //Minimum feasible value for missing values. 
  //If you don't have prior information about this set it to -1/0
  Real missing.lowerBound, 
  //Maximum feasible value for missing values. 
  //If you don't have prior information about this set it to +1/0
  Real missing.upperBound)
//////////////////////////////////////////////////////////////////////////////
{
  Text g = Grammar(serie);
  Case(
  g=="Serie", 
  {
  //WriteLn("TRACE [Obs.Data.Info] 1");
    Obs.Serie.Info(
      id_node, name, ownerType, serie, numIniVal, first, last, missing.label,
      missing.sigmaFactor, missing.lowerBound, missing.upperBound)
  },
  g=="Matrix",
  {
  //WriteLn("TRACE [Obs.Data.Info] 2");
    Obs.Vector.Info(
      id_node, name, ownerType, serie, missing.label,
      missing.sigmaFactor, missing.lowerBound, missing.upperBound)
  },
  {
    WriteLn("[Obs.Data.Info] Invalid type "+g+" should be Serie or Matrix","E");
    Real Stop
  })
};

//////////////////////////////////////////////////////////////////////////////
//Returns full information about a time serie that will be used as input of 
//a dynamic observational node
NameBlock Obs.Input(
  //Parameter identifier
  Text id_parameter,
  //The result of calling Build.Serie.info for an input serie
  NameBlock serie.info,
  //Initial value for the regression parameter
  Real initValue, 
  //Minimum feasible value for regression parmeter
  //If you don't have prior information about this set it to -1/0
  Real param.lowerBound, 
  //Maximum feasible value for regression parmeter
  //If you don't have prior information about this set it to +1/0
  Real param.upperBound)
//////////////////////////////////////////////////////////////////////////////
{[[
  NameBlock _.serie.info=serie.info;
  Set _.param = @Bsr.Param.Info(
    "LinearBlk::"+_.serie.info::_.id_node+"::"+id_parameter, 
    initValue, 
    param.lowerBound, 
    param.upperBound)
]]};


//////////////////////////////////////////////////////////////////////////////
//Returns full information about a time serie that will be used as input of 
//a dynamic observational node
NameBlock Obs.TransFun(
  //Node identifier
  Text id_node,
  //TransferFunction identifier
  Text id_transferFunction,
  //Numerator polynomial
  Text te_omega,
  //Denominator polynomial
  Text te_delta,
  //The result of calling Build.Serie.info for an input serie
  NameBlock serie.info,
  //Initial value for the regression parameter
  Real initValue, 
  //Minimum feasible value for regression parmeter
  //If you don't have prior information about this set it to -1/0
  Real param.lowerBound, 
  //Maximum feasible value for regression parmeter
  //If you don't have prior information about this set it to +1/0
  Real param.upperBound)
//////////////////////////////////////////////////////////////////////////////
{[[
  NameBlock _.serie.info=serie.info;
  Text _.transferFunction = id_transferFunction;
  Polyn _.omega = Eval(te_omega);
  Polyn _.delta = Eval(te_delta);
  Real _.p = Degree(_.omega);
  Real _.q = Degree(_.delta);
  Real _.h = Max(_.p, _.q);
  Real _.hasDelta = _.q>0;
//Real _trz.1 = { WriteLn("TRACE [Obs.TransFun] 1 "+id_node+"::"+_.transferFunction), 0 };
//Real _trz.2 = { WriteLn("TRACE [Obs.TransFun] 2 p="<<_.p+" q="<<_.q+" h="<<_.h+" hasDelta="<<_.hasDelta), 0};
  Set _.omegaParam = EvalSet(Monomes(_.omega), Set omegaBlk(Polyn mon)
  {
    @Bsr.Param.Info(
      "OmegaBlk::"+_.serie.info::_.id_node+"::"+id_transferFunction+
      "::NumDeg."+IntText(Degree(mon)), 
      initValue, 
      param.lowerBound, 
      param.upperBound)
  });
  Set _.deltaParam = EvalSet(Monomes(1-_.delta), Set deltaBlk(Polyn mon)
  {
    Real deg = Degree(mon);
    @Bsr.Param.Info(
      "DeltaBlk::"+_.serie.info::_.id_node+"::"+id_transferFunction+
      "::DenDeg."+IntText(deg), 
      0, 
      -deg, 
      +deg)
  });
  Set _.denIniParam = For(1,_.h, Set denIni(Real lag)
  {
    @Bsr.Param.Info(
      "DeltaBlk::"+_.serie.info::_.id_node+"::"+id_transferFunction+
      "::DenIni."+IntText(_.h-lag), 
      0, 
      -1/0, 
      +1/0)
  });
  NameBlock _.deltaNonLinBlock = If(Not(_.hasDelta), UNKNOWN,
  {
    Serie ser = _.serie.info::_.serie;
    Date fst0 = _.serie.info::_.firstIniVal;
    Date lst0 = _.serie.info::_.lastIniVal;
    Date fst  = _.serie.info::_.firstDate;
    Date lst  = _.serie.info::_.lastDate;

    BysMcmc::Bsr::Gibbs::DeltaTransfer
    (
      id_node+"::Noise",
      Copy(_.transferFunction),
      EvalSet(_.omegaParam, Text(@Bsr.Param.Info pi) { pi->Name}),
      Copy(_.omega), 
      Copy(_.delta),
      If(!_.p,Rand(0,1,0,0), Tra(SerSetMat([[ser]], fst0, lst0))),
      Tra(SerSetMat([[ser]], fst,lst))
    )
  })
]]};


//////////////////////////////////////////////////////////////////////////////
//Returns full information about a time serie that will be used as output of 
//a dynamic observational node
NameBlock Obs.Output(
  //The result of calling Build.Serie.info for an input serie
  NameBlock serie.info,
  Real sigma,
  Real sigmaPriorWeight,
  //A possibly empty set of ARIMA factors with structure @ARIMAStruct
  Set arima)
//////////////////////////////////////////////////////////////////////////////
{[[
  NameBlock _.serie.info=serie.info;
  Real _.sigma = sigma;
  Real _.sigmaPriorWeight = sigmaPriorWeight;
  Set _.arima.factors = arima
]]};

//////////////////////////////////////////////////////////////////////////////
//Returns the NameBlock needed to build BSR information about an dynamic
//observational node 
NameBlock Obs(
  //The result of calling Obs.Output
  NameBlock output, 
  //A set where each item is the result of calling Obs.Input
  Set input,
  //A set where each item is the result of calling Obs.TransFun
  Set transFun,
  //Acumulate input.missing.labels to avoid repetitions
  Set input.missing.labels,
  //Observational node must be in a primary or joint module 
  Text module.type,
  //The result of calling BysMcmc::Bsr::Import::Constraints
  NameBlock constraints,
  Real forceZeroInputCoef,
  Set genericNonLinearFilters)
//////////////////////////////////////////////////////////////////////////////
{[[
  //Optional ad-hoc API.
  Text _.id_node = output::_.serie.info::_.id_node;
  Text _.series = output::_.serie.info::_.name;
  NameBlock _.output = output;
  Set _.input = input;
  Set _.transFun = transFun;
  Set _.transFun.omega = SetConcat(EvalSet(_.transFun, Set(NameBlock tf)
  {
    tf::_.omegaParam
  }));
  Set _.transFun.serie = SetConcat(EvalSet(_.transFun, Set(NameBlock tf)
  {
    EvalSet(Monomes(tf::_.omega), NameBlock(Polyn mon)
    {[[
      NameBlock serie = tf::_.serie.info;
      Real degree = Degree(mon)
    ]]})
  }));
  Set _.genericNonLinearFilters = genericNonLinearFilters;
  Set _.deltaNonLinBlk = 
  {
    Set sel = Select(_.transFun,Real(NameBlock tf)
    {
    //WriteLn("TRACE Obs Degree("+tf::_.transferFunction+"::_.delta="<<tf::_.delta+")="<<Degree(tf::_.delta));
      tf::_.hasDelta
    });
  //WriteLn("TRACE Obs Card(sel)="<<Card(sel));
    EvalSet(sel, NameBlock(NameBlock tf)
    {
      tf::_.deltaNonLinBlock
    })
  };

  Real _.nInp = Card(_.input);
  Real _.nTfo = Card(_.transFun.omega);

  NameBlock _.constraints = constraints;
  Real _.forceZeroInputCoef = forceZeroInputCoef;
  Text _.module.type = module.type;
  Set _.missing = _.output::_.serie.info::_.missing << 
    SetConcat(For(1,_.nInp, Set(Real j)
    {
      NameBlock inp = _.input[j];
    //WriteLn("TRACE 1 inp::_.serie.info::_.missing = "<<inp::_.serie.info::_.missing);
    //WriteLn("TRACE 2 input.missing.labels = "<<input.missing.labels);
      Set aux = If(Or(_.module.type=="primary",
                      inp::_.serie.info::_.id_node ==
                      inp::_.serie.info::_.missing.label),
      {
        inp::_.serie.info::_.missing
      },
      {
        Select(inp::_.serie.info::_.missing, Real(BysMcmc::@Bsr.Missing.Info mis)
        {
          Text name = Name(mis);
          Real idx = FindIndexByName(input.missing.labels,name);
        //WriteLn("TRACE 3 name = "<<name+" idx = "<<idx);
        //WriteLn("TRACE Obs Missing Label Name : "+label[1]+" -> "<<idx);
          !idx
        })
      });
    //WriteLn("TRACE 4 aux = "<<aux);
      Real If(Or(_.module.type=="primary", !Card(aux)), 0,
      {
        Set Append(input.missing.labels, aux);
      //WriteLn("TRACE 5 input.missing.labels = "<<input.missing.labels);
        Real SetIndexByName(input.missing.labels);
        0
      });
      Set EvalSet(aux, Real(BysMcmc::@Bsr.Missing.Info mis)
      {
        Real mis->Owner.Column := j
      });
      aux
    }));

  //Mandatory methods that will be used by BysMcmc::Bsr::Import

  //Returns the unique identifier of the regression segment
  Text Get.Name(Real unused) { _.id_node };

  /////////////////////////////////////////////////////////////////////////
  // MAIN LINEAR BLOCK
  Real Get.Param.Size(Real unused) { _.nInp + _.nTfo };
  Set Get.Param(Real numParam) 
  { 
    If(numParam<=_.nInp, 
      (_.input[numParam])::_.param,
      _.transFun.omega[numParam-_.nInp])
  };
  Real Get.Param.ForceZero(Real term) { term > _.nInp };
  /////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////
  // MISSING VALUES BLOCK
  Real Get.Missing.Size(Real unused) {  Card(_.missing) };
  Set Get.Missing(Real numMissing) { _.missing[numMissing] };
  /////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////
  // SIGMA BLOCK
  //Returns a Real fixed known sigma value as prior information or a 
  //Text variable name to be simulated in the Sigma Gibbs block of BSR

  Real hasPrior = If(IsUnknown(_.output::_.sigmaPriorWeight),False,
                     LT(0,_.output::_.sigmaPriorWeight,1));
  Anything Get.Sigma2(Real unused) 
  { 
    If(IsUnknown(_.output::_.sigma) | hasPrior,
      "SigmaBlk::"+_.id_node+"::Variance",
      _.output::_.sigma^2)
  };
  Text Get.SigmaPrior(Real unused) 
  { 
    If(!hasPrior,"",
       "@Bsr.Sigma.Prior.InverseGamma("<<
       _.output::_.sigma+","<<_.output::_.sigmaPriorWeight+")")
  };

  /////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////
  // ARIMA BLOCK
  //Returns the number of ARIMA factors for a regression segment about
  //a discrete time process. If there is not a time process or is a 
  //white noise process then returns 0
  Real Get.ARIMA.Size(Real unused) { Card(_.output::_.arima.factors) };
  //Returns the f-th ARIMA factor with structure @ARIMAStruct
  //In current version all AR or MA factors must have degrees 0, 1 or 2
  //In order to define higher degrees you will need to add more 
  //@ARIMAStruct items
  Set Get.ARIMA.Factor(Real f) { _.output::_.arima.factors[f] };
  /////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////
  // NON LINEAR FILTERS BLOCK
  //Returns a set of NameBlock's with user specified non linear filters
  Set Get.NonLinearFilters(Real unused)
  {
    Set aux = _.deltaNonLinBlk << _.genericNonLinearFilters;
  //WriteLn("TRACE 1 Obs::Get.NonLinearFilters 1 Card(aux) = "<<Card(aux));
    aux
  };
  /////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////
  // REGRESSION EQUATIONS
  Set Get.TimeInfo(Real unused)
  {
    If(Not(_.output::_.serie.info::_.isTimeSerie),Copy(Empty),
    @BSR.NoiseTimeInfo
    (
      Dating(_.output::_.serie.info::_.serie),
      _.output::_.serie.info::_.firstDate,
      _.output::_.serie.info::_.lastDate
    ))
  };

  VMatrix Get.OutputVMatrix(Real unused)
  {
    Mat2VMat(_.output::_.serie.info::Get.Matrix(0))
  };

  VMatrix Get.InputVMatrix(Real unused)
  {
    Set inp = EvalSet(_.input, VMatrix(NameBlock aux)
    {
      Mat2VMat(aux::_.serie.info::Get.Matrix(0))
    });
    Set trf = EvalSet(_.transFun.serie, VMatrix(NameBlock aux)
    {
      Mat2VMat(aux::serie::Get.Matrix(aux::degree))
    });
    If(!Card(inp) & !Card(trf), Rand(0,0,0,0),
       Group("ConcatColumns", inp << trf))
  };

  Real Get.Equation.Size(Real unused) 
  { 
    _.output::_.serie.info::Get.Equation.Size(0)
  };
  //Returns Real known output or Text missing name
  Text Get.Equation.Output(Real t)
  {
    _.output::_.serie.info::get.coef(t)
  };
  //Returns the number of input terms involved in t-th equation
  Real Get.Equation.Input.Size(Real t) { _.nInp + _.nTfo };
  //Returns Real known output or Text missing name of an input term
  Text Get.Equation.Input.Coef(Real t, Real term)
  {
    If(term<=_.nInp, 
    {
      (_.input[term])::_.serie.info::get.coef(t)
    },
    {
      NameBlock aux = _.transFun.serie[term-_.nInp];
      aux::serie::get.knownCoefOrAvg(t-aux::degree)
    })
  };
  //Returns the param name of an input term
  Text Get.Equation.Input.Param(Real numEqu, Real term)
  {
    If(term<=_.nInp, 
    {
      ((_.input[term])::_.param)->Name
    },
    {
      _.transFun.omega[term-_.nInp]->Name
    })
  };

  ///////////////////////////////////////////////////////////////////////////
  // LINEAR CONSTRAINING INEQUATIONS
  NameBlock Get.Constraints.Handler(Real unused) { _.constraints }

]]};

