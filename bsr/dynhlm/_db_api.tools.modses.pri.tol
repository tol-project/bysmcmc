//////////////////////////////////////////////////////////////////////////////
// FILE    : _db_api.tools.pri.tol
// PURPOSE : Implements database tools related methods of NameBlock 
//           BysMcmc::Bsr::DynHlm::DBApi
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//Inserts a new latent equation register
Real ModSes.Pri.Equ.Create(Text dbName, 
                           Text model, 
                           Text session,
                           Text node_father,
                           Real numEqu,
                           Real average,
                           Real sigma,
                           Real sigmaPriorWeight)
//////////////////////////////////////////////////////////////////////////////
{
  NameBlock _table = BysMcmc::Bsr::DynHlm::DBApi::_.table;
  Real ok = BysMcmc::Bsr::DynHlm::DBApi::SqlCachedInsert(
  dbName+"."+_table::PriEqu,
  "'"+model+"','"+session+"',"
  "'"+node_father+"',"+
  IntText(numEqu)+","+
  SqlFormatReal(average)+","+
  SqlFormatReal(sigma)+","+
  SqlFormatReal(sigmaPriorWeight));
  Eval("ModSes.Pri.Equ.Create.ok=ok")
};

//////////////////////////////////////////////////////////////////////////////
//Inserts a new prior output register
Real ModSes.Pri.Output.Create(Text dbName, 
                              Text model, 
                              Text session,
                              Text node_father,
                              Real numEqu,
                              Text node_child,
                              Text parameter_child,
                              Real coef)
//////////////////////////////////////////////////////////////////////////////
{
  NameBlock _table = BysMcmc::Bsr::DynHlm::DBApi::_.table;
  Real ok = BysMcmc::Bsr::DynHlm::DBApi::SqlCachedInsert(
  dbName+"."+_table::PriOutput,
  "'"+model+"','"+session+"',"
  "'"+node_father+"',"+
  IntText(numEqu)+","+
  "'"+node_child+"','"+parameter_child+"',"+
  SqlFormatReal(coef));
  Eval("ModSes.Pri.Output.Create.ok=ok")
};

//////////////////////////////////////////////////////////////////////////////
Real ModSes.Pri.Homog_1.Create(Text dbName, 
                               Text model, 
                               Text session,
                               Text id_node,
                               Text description,
                               Set  node_childs)
//////////////////////////////////////////////////////////////////////////////
{
  Real ok = If(Card(node_childs)<1,
  {
    Text aux = If(Card(node_childs)," but one "," "); 
    WriteLn("Cannot create prior node "+id_node+" due to related input"
    " is disabled along all"+aux+"child nodes","W");
    0
  },
  {
    WriteLn("Creating prior node "+id_node+" for "<<Card(node_childs)+
            " observational nodes");
    Real ok.1 = ModSes.Node.Create
    (
      dbName, id_model, id_session, id_node, 
      "PRI",
      888888888,
      1,
      description
    );
    Real ok.2 = SetMin(For(1,Card(node_childs), Real pri.equ(Real k)
    {
      NameBlock child = node_childs[k];
      Real sigmaPriorWeight = If(ObjectExist("Real","child::_.sigmaPriorWeight"),
        child::_.sigmaPriorWeight, ?);
      Real ModSes.Pri.Equ.Create(
        dbName, model, session,id_node,k,
        child::_.average,
        child::_.sigma,
        sigmaPriorWeight);
      Real ModSes.Pri.Output.Create(
        dbName, model, session,id_node,k,
        child::_.id_node,
        child::_.id_parameter,
        1.0)
    }));
    And(ok.1,ok.2)
  });
  Eval("ModSes.Pri.Homog_1.Create."+id_node+".ok=ok")
};
//////////////////////////////////////////////////////////////////////////////
/*
Implements an a priori node for combinatorial output:

/Sum_j Y^{ij}*beta_j ~ N (mu_i, sigma_i).  j = 1, ....., m-parameters
                                           i = 1, .... , n-node equations

Relevant arguments are:

Set PriorInfo: Set of NameBlocks, specifies the averages and sigma's of each node 
               equation 
               Set PriorInfo = Set de NameBlocks 
                             [[

              NameBlock equ1 = [[ _.average, _.sigma, _.sigmaPriorWeight ]]
              NameBlock equ2 = [[ _.average, _.sigma, _.sigmaPriorWeight ]]
              ...
                              ]]

Set node_childs: Set Of NameBlocks, specifies the node and parameter's name of
                 each beta_j

 Set  node_childs = [[

       NameBlock  beta1 = [[  Text _.id_node =   , Text _.id_parameter=   ]],
       NameBlock  beta2 = [[  Text _.id_node =   , Text _.id_parameter=   ]],
       ................
       NameBlock  betaj = [[  Text _.id_node =   , Text _.id_parameter=   ]]
     
                    ]]   

*/
Text _.autoDoc.member.ModSes.Pri.Out.Comb.Create =
"
Implements an a priori node for combinatorial output:

/Sum_j Y^{ij}*beta_j ~ N (mu_i, sigma_i).  j = 1, ....., m-parameters
                                           i = 1, .... , n-node equations

Relevant arguments are:

Set PriorInfo: Set of NameBlocks, specifies the averages and sigma's of each node 
               equation 
               Set PriorInfo = Set de NameBlocks 
                             [[

              NameBlock equ1 = [[ _.average, _.sigma, _.sigmaPriorWeight ]]
              NameBlock equ2 = [[ _.average, _.sigma, _.sigmaPriorWeight ]]
              ...
                              ]]

Set node_childs: Set Of NameBlocks, specifies the node and parameter's name of
                 each beta_j

 Set  node_childs = [[

       NameBlock  beta1 = [[  Text _.id_node =   , Text _.id_parameter=   ]],
       NameBlock  beta2 = [[  Text _.id_node =   , Text _.id_parameter=   ]],
       ................
       NameBlock  betaj = [[  Text _.id_node =   , Text _.id_parameter=   ]]
     
                    ]]";

Real ModSes.Pri.Out.Comb.Create(Text dbName, 
                               Text model, 
                               Text session,
                               Text id_node,
                               Text description,
                               Real number,  
                               Set  PriorInfo, 
                               Set  node_childs, //Set de NameBlocks 
                               Matrix Y)
//////////////////////////////////////////////////////////////////////////////
{
  Real ok = If(Card(node_childs)<1,
  {
    Text aux = If(Card(node_childs)," but one "," "); 
    WriteLn("Cannot create prior node "+id_node+" due to related input"
    " is disabled along all"+aux+"child nodes","W");
    0
  },
  {
    WriteLn("Creating latent node "+id_node+" for "<<Rows(Y)+
            " observational nodes");
    Real ok.1 = ModSes.Node.Create
    (
      dbName, id_model, id_session, id_node, 
      "PRI",
      888888888,
      number,
      description
    );

    Real ok.2 = SetMin ( For (1, Card(PriorInfo), Real (Real i){//ciclo ecuaciones nodo
            NameBlock PInfo = PriorInfo[i];   
            Real sigmaPriorWeight = If(ObjectExist("Real","PInfo::_.sigmaPriorWeight"),
                                                PInfo::_.sigmaPriorWeight, ?);
            Real ModSes.Pri.Equ.Create (dbName, model, session, id_node,
                      i,
                      PInfo::_.average,
                      PInfo::_.sigma,
                      sigmaPriorWeight);
                      
             Set For (1, Card(node_childs), Real (Real j){
                  NameBlock child = node_childs[j];
                  Real If (MatDat(Y,i,j)==0,0,    
                    Real ModSes.Pri.Output.Create(dbName, model, session,
                     id_node,
                     i,
                     child::_.id_node, 
                     child::_.id_parameter,
                     MatDat(Y,i,j)) )

              }); 
           1
        })); // Fin por ecuaciones nodo
        And(ok.1,ok.2) 
   });  //Fin If ok
     Eval("ModSes.Pri.Out.Comb.Create."+id_node+".ok=ok")
}; // Fin de la funcion ModSes.Pri.Out.Comb.Create
