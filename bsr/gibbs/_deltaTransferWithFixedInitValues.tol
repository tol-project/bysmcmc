/////////////////////////////////////////////////////////////////////////////
// LIBRARY: StdtLib (Standard Library of TOL)
// FILE: _deltaTransferWithFixedInitValues.tol
// PURPOSE: Declares method BysMcmc::Bsr::Gibbs::RatioTransferWithFixedInitValues
/////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Text _.autodoc.member.DeltaTransferWithFixedInitValues =
"Builds a NameBlock that can be used as non linear filter by method "
"NonLinBlock in order to simulate delta polynomials of transfer functions "
"given by differential equations\n"
"  delta(B)*z = omega(B)*x          if hasUnitGain is false\n"
"  delta(B)*z = delta(1)*omega(B)*x if hasUnitGain is true\n"
"Parameters are delta coefficients"
"Initial values of z are taken as ceroes\n"
"Arguments are:\n"
"  Text   segmentName : identifier of affected output segment\n"
"  Text   inputName   : identifier of affectd input group\n"
"  Set    linBlkNames : identifiers of affected linear block parameters\n"
"  Polyn  omega       : structure of numerator of transfer function\n" 
"  Polyn  delta       : structure of numerator of transfer function\n"
"  Matrix x0          : initial values of original input\n"
"  Matrix x           : original input data\n"
"  Real   hasUnitGain : If true omega coeffients will be divided by delta(1)\n"
"";
//////////////////////////////////////////////////////////////////////////////
NameBlock DeltaTransferWithFixedInitValues
(
  Text   segmentName,
  Text   inputName,
  Set    linBlkNames,
  Polyn  omega, 
  Polyn  delta,
  Matrix x0,
  Matrix x,
  Real   hasUnitGain
)
//////////////////////////////////////////////////////////////////////////////
{[[
  //Identifier of segment
  Text _.segmentName = segmentName;
  //Identifier of original input
  Text _.inputName = inputName;
  //Name of linear parameters affected by this filter (omega)
  Set _.linBlkNames = linBlkNames;
  //Numerator polynomial is needed just for structural questions but 
  //coefficients are irrelevant
  Polyn _.omega = omega;
  //Denominator polynomial is the main tarjet of this filter 
  Polyn _.delta = delta;
  //Periodicity is extracted from delta as the lest non null coefficient 
  //So delta must be a full filled polynomial
  Real  _.periodicity = Degree(Monomes(_.delta)[2]); 
  //Initial values of original input
  Matrix _.x0 = x0;
  //Original input
  Matrix _.x = x;

  Real   _.hasUnitGain = hasUnitGain;

  Real _.m = Rows(x);
  Real _.p = Degree(_.omega);
  Real _.q = Degree(_.delta);
  Real _.ps = _.p/_.periodicity;
  Real _.qs = _.q/_.periodicity;
  Matrix _.y   = _.x;
  Matrix _.y0 = Rand(Max(_.p,_.q),1,0,0);
  Set _.omegaDeg = EvalSet(Monomes(_.omega), Degree);
  Set _.deltaDeg = EvalSet(Monomes(_.delta), Degree);
  Real _.omegaSize = Card(_.omegaDeg);
  Real _.deltaSize = Card(_.deltaDeg);
  Text _.cleanSegmentName = Replace(_.segmentName,"::Noise","");
  Text _.name = "DeltaBlk::"+_.cleanSegmentName+"::"+_.inputName;
  Set _.denDegNames = EvalSet(Monomes(1-_.delta),Text(Polyn mon)
  {
    _.name+"::DenDeg."+IntText(Degree(mon))
  });
  Set _.colNames = _.denDegNames;

  ///////////////////////////////////////////////////////////////////////////
  Real initialize(Real unused)
  ///////////////////////////////////////////////////////////////////////////
  {
    Real ok = Case(
    Card(_.linBlkNames) != _.omegaSize,
    {
      WriteLn("DeltaTransfer "+_.segmentName+"::"+_.inputName+
      " has "<<_.omegaSize << " monomes in numerator ("<<_.omega+") but argument "<<
      "Set linBlkNames has "<<Card(_.linBlkNames)<<" elements","E");
      Real Stop;
      False
    },
    _.qs > 2,
    {
      WriteLn("DeltaTransfer "+_.segmentName+"::"+_.inputName+
      " has degree of denominator ("<<_.delta+") "<<_.q << ">2*"<<_.periodicity,"E");
      Real Stop;
      False
    },
    _.q <= 0,
    {
      WriteLn("DeltaTransfer "+_.segmentName+"::"+_.inputName+
      " has degree of denominator ("<<_.delta+") "<<_.q << "<=0","E");
      Real Stop;
      False
    },
    _.deltaSize-1 != _.qs, 
    {
      WriteLn("DeltaTransfer "+_.segmentName+"::"+_.inputName+" has non "
        "full filled denominator ("<<_.delta+")","E");
      Real Stop;
      False
    },
    1==1, True);
    ok
  };

  //Auxiliar variables used to skip zeroes in delta coefficients
  Matrix _one.qs.1 = Constant(_.qs,1,1);
  Matrix _eps.qs.1 = Constant(_.qs,1,0.000001);

  ///////////////////////////////////////////////////////////////////////////
  Real set.parameter(Matrix paramValues)
  ///////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE DeltaTransfer()::set.parameter 1");
    Matrix delta_ = Sub(paramValues, 1, 1, _.qs, 1);
    Matrix delta  = IfMat(Eq(_one.qs.1,delta_+1), _eps.qs.1, delta_);
    Polyn _.delta := 1-(B^_.periodicity)*MatPol(Tra(delta));
    1
  };

  ///////////////////////////////////////////////////////////////////////////
  Matrix get.parameter(Real unused)
  ///////////////////////////////////////////////////////////////////////////
  {
    PolMat(F*(1-ChangeBDegree(_.delta,1/_.periodicity)),_.qs,1)
  };

/////////////////////////////////////////////////////////////////////////////
//Mandatory methods for all non linear filters
/////////////////////////////////////////////////////////////////////////////
  //Identifies the filter
  Text get.name       (Real unused) { _.name };
  //Identifies the segment
  Text get.segmentName(Real unused) { _.segmentName };
  //Parameters of non linear block
  Set  get.colNames   (Real unused) { _.colNames };
  //Parameters of linear block. This method must exist just for input filters
  Set  get.linBlkNames(Real unused) { _.linBlkNames };

  ///////////////////////////////////////////////////////////////////////////
  Set get.bounds(Real paramIdx, Matrix paramValues) 
  //Returns left and right bounds for i-th \n parameter for current values of 
  //the rest of them
  ///////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE DeltaTransfer()::get.bounds");
    Real set.parameter(paramValues);
  
    ARMAProcess::StationarityBounds.2(
      _.delta, _.periodicity, i*_.periodicity)
  };

  ///////////////////////////////////////////////////////////////////////////
  Matrix eval(Matrix paramValues)
  //Returns the filter matrix
  ///////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE DeltaTransfer()::eval");
    Real set.parameter(paramValues);
  //WriteLn("TRACE DeltaTransfer()::eval _.delta="<<_.delta);
    Real gainCoef = If(_.hasUnitGain, EvalPol(_.delta,1), 1);
  //WriteLn("TRACE DeltaTransfer()::eval y=("<<Replace(""<<Matrix Tra(y),"\n","")+")");
    Matrix Y = Group("ConcatColumns",EvalSet(_.omegaDeg,Matrix(Real deg)
    {
      DifEq((gainCoef*B^deg)/_.delta, _.x, _.x0, _.y0)
    }));
    Y
  }

]]};
